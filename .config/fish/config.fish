# Set PATH
# set --export PATH $PATH
set --export PATH $PATH ~/bin

# Environment variables
function setenv
    set -gx $argv
end
source ~/.env
