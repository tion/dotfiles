# Description

Collection of user dot files.

## Usage

Simple copy/paste into your home directory in the same structure as it appears here. Please note, a lot of the files are highly customized and may require modifications to work with your system (for example to work with systemd vs other inits).
