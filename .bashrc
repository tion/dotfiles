# ~/.bashrc

function setenv() { export "$1=$2"; }
. ~/.env

# Add ~/bin to PATH for my executables
#export PATH="$PATH:$HOME/bin:$HOME/go/bin:/opt/texlive/2017/bin/x86_64-linux"
export MANPATH="$MANPATH:/opt/texlive/2017/texmf-dist/doc/man"
export INFOPATH="$INFOPATH:/opt/textlive/2017/texmf-dist/doc/info"

# Launch when terminal is opened
# last does not work as expected with Void
# Does not list user as being logged in even
# it is /shrug
#ll=$(last -1 -F -R  $USER | head -1 | cut -c 20-)
#echo "last login: $ll"
cat /dev/null > ~/.bash_history && history -cw

# Add nano or micro as default editor
export EDITOR=micro
export TERMINAL=urxvt
export BROWSER=icecat

# For rtv
#export BROWSER=w3m
export RTV_EDITOR=micro
export RTV_URLVIEWER=urlview
export RTV_BROWSER=firefox

# If not running interactively, don't do anything
[ -z "$PS1" ] && return

# Taken from raspian bashrc
# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignorespace:ignoredups

# Unset history expansion permanently
# Useful for things like echo "Hello World!"
# but does not allow for expanding history like: !!:s/51/52
#set +o histexpand

# Set history expansion
set -H

# Prompts
# PS1 -primary prompt before each command
GREEN="\[$(tput setaf 2)\]"
RESET="\[$(tput sgr0)\]"

# Bash Prompt: depending on exit code of last command executed
# Taken/modified from: http://stackoverflow.com/questions/16715103/bash-prompt-with-last-exit-code#16715681
export PROMPT_COMMAND=__prompt_command  # Func to gen PS1 after CMDs
function __prompt_command() {
    local EXIT="$?"             # This needs to be first
#    PS1="\n[ \@ - \d ][ \u:\h ][ \w ]\n  "
#    PS1="\n[ \u ] [ \w ]\n  "

    local RCol='\[\e[0m\]'

    local Red='\[\e[0;31m\]'
    local Gre='\[\e[0;32m\]'
    local Yel='\[\e[0;33m\]'
    local Blu='\[\e[0;34m\]'
    local BYel='\[\e[1;33m\]'
    local BBlu='\[\e[1;34m\]'
    local Pur='\[\e[0;35m\]'

#    PS1="\n${RCol}« ${Yel}\u${RCol} • ${Pur}\w${RCol} »\n  "
#    PS1="\n« ${Yel}\@${RCol} • ${Pur}\w${RCol} »\n"
    PS1="« ${Pur}\w${RCol} »\n"

    if [ $EXIT != 0 ]; then
#	PS1+="${Red}(°□°）${RCol}"
#        PS1+="${Red}(╯°□°）╯︵ ┻━┻${RCol}"      # Add red if exit code non 0
#	PS1+="[${Red}✘${RCol}]"
	#PS1+="[${Red}✗${RCol}]"
	PS1+="[${Red}\#${RCol}]"
    else
#        PS1+="${Gre}(◡‿◡✿)${RCol}"
#	PS1+="${Gre}(⌐■_■)${RCol}"
#	PS1+="[${Gre}✔${RCol}]"
	#PS1+="[${Gre}✓${RCol}]"
	PS1+="[${Gre}\#${RCol}]"
    fi

#    PS1+="${RCol} (\#) λ "
    PS1+="[${Yel}\!${RCol}]${BBlu} λ${RCol} "
}
# PS2 - secondary prompt before multi-line command
export PS2="${GREEN}  ~>\[\e[0;34m\]${RESET} "
# Gtk themes
export GTK2_RC_FILES="$HOME/.gtkrc-2.0"

# Load aliases from ~/.bash_aliases
if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# Automatically search the official repositories, when entering 
# an unrecognized command
#source /usr/share/doc/pkgfile/command-not-found.bash

# Line wrap on window resize
shopt -s checkwinsize

#-------------------------------------------------------------
# Adds some text in the terminal frame (if applicable).
# Taken from: http://tldp.org/LDP/abs/html/sample-bashrc.html
#-------------------------------------------------------------
function xtitle()
{
    case "$TERM" in
    *term* | rxvt)
        echo -en  "\e]0;$*\a" ;;
    *)  ;;
    esac
}

#-------------------------------------------------------------
# Tailoring 'less'
# Taken from: http://tldp.org/LDP/abs/html/sample-bashrc.html
#-------------------------------------------------------------

alias more='less'
export PAGER=less
export LESSCHARSET='latin1'
export LESSOPEN='|/usr/bin/lesspipe.sh %s 2>&-'
                # Use this if lesspipe.sh exists.
export LESS='-i -N -w  -z-4 -g -e -M -X -F -R -P%t?f%f \
:stdin .?pb%pb\%:?lbLine %lb:?bbByte %bb:-...'

# LESS man page colors (makes Man pages more readable).
export LESS_TERMCAP_mb=$'\E[01;31m'
export LESS_TERMCAP_md=$'\E[01;31m'
export LESS_TERMCAP_me=$'\E[0m'
export LESS_TERMCAP_se=$'\E[0m'
export LESS_TERMCAP_so=$'\E[01;44;33m'
export LESS_TERMCAP_ue=$'\E[0m'
export LESS_TERMCAP_us=$'\E[01;32m'
PATH=$PATH:/tmp/tools/arm-bcm2708/gcc-linaro-arm-linux-gnueabihf-raspbian-x64/bin
